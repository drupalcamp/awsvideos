Drupal 7 module: awsvideos 
- Upload a video to the Drupal 7 site;
- Run FFMPEG to convert to MP4 and Stream Videos;
- Upload the video to AWS S3 bucket via AWS API.